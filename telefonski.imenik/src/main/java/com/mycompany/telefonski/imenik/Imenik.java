package com.mycompany.telefonski.imenik;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Imenik {

    Scanner t = new Scanner(System.in);

    private List<Kontakt> imenik;

    public List<Kontakt> getImenik() {
        return imenik;
    }

    public Imenik() {
        imenik = new ArrayList<>();
    }

    public void prikaziImenik() {
        System.out.println("Sadržaj imenika:");
        System.out.println(imenik.toString());
    }

    public void dodajKontakt(Kontakt kontakt) {
        this.imenik.add(kontakt);
    }

    public void pregledImenika() {
        System.out.println("Izvrsite pretragu imenika po imenu i prezimenu ili po broju:");
        String pretraga = t.nextLine();
        int brojac=0;
        for (int i = 0; i < imenik.size(); i++) {
            Kontakt k = imenik.get(i);
            if ((k.getIme()+" "+ k.getPrezime()).equals(pretraga) || k.getTel().contains(pretraga)) {
                System.out.println("Imenik sadrzi kontakt:");
                System.out.println(k);
                brojac++;
            }
        }
        if(brojac==0){
            System.out.println("Trazeni kontakt ne postoji!");
        }

    }

    public void brisanjeKontakta() {
        System.out.println("Unesite ime i prezime kontakta koji želite da obrišete:");
        String brisanje = t.nextLine();
        int brojac=0;
        for(int i=0; i<imenik.size(); i++){
            Kontakt k = imenik.get(i);
            if((k.getIme()+" "+k.getPrezime()).equals(brisanje)){
                imenik.remove(k);
                System.out.println("Obrisan kontakt "+k.getIme());
                brojac++;
            }
        }
        if(brojac==0){
            System.out.println("Unešeni kontakt za brisanje ne postoji!");
        }
    }
}
