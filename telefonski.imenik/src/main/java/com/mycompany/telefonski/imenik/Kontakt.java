package com.mycompany.telefonski.imenik;

import java.util.HashSet;
import java.util.Set;
import org.w3c.dom.Element;

public class Kontakt {
    
    private String ime;
    private String prezime;
    private String adresa;
    private Set<String> tel;
    private Set<String> email;
    
    {
        this.tel = new HashSet<>();
        this.email = new HashSet<>(); 
    }

    public Kontakt() {
    }
    
    public Kontakt(Element e){
        this.ime = e.getElementsByTagName("ime").item(0).getTextContent();
        this.prezime = e.getElementsByTagName("prezime").item(0).getTextContent();
        this.adresa = e.getElementsByTagName("adresa").item(0).getTextContent();
        Set<String> telefon = new HashSet<>();
        int brojElemenata = e.getElementsByTagName("tel").getLength();
        for(int i=0; i<brojElemenata; i++){
            String telf = e.getElementsByTagName("tel").item(0).getTextContent();
            telefon.add(telf);
            this.tel = telefon;
        }
        Set<String> mail = new HashSet<>();
        int brojElemenataMail = e.getElementsByTagName("email").getLength();
        for(int i=0; i < brojElemenataMail; i++) {
            String emailUnos = e.getElementsByTagName("email").item(i).getTextContent();
            mail.add(emailUnos);
            this.email = mail;
        }
    }

    public Kontakt(String ime, String prezime, String adresa, Set<String> tel, Set<String> email) {
        this.ime = ime;
        this.prezime = prezime;
        this.adresa = adresa;
        this.tel = tel;
        this.email = email;
    }    
    
    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public Set<String> getTel() {
        return tel;
    }

    public void setTel(Set<String> tel) {
        this.tel = tel;
    }

    public Set<String> getEmail() {
        return email;
    }

    public void setEmail(Set<String> email) {
        this.email = email;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        sb.append(ime);
        sb.append(" ");
        sb.append(prezime);
        sb.append(":");
        sb.append(")");
        return sb.toString();
    }   
}