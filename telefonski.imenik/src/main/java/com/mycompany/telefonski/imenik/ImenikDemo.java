package com.mycompany.telefonski.imenik;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.json.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class ImenikDemo {

    private static final Imenik imenik = new Imenik();

    private static final String xmlPutanja = "C:\\Users\\NIKOLA\\Desktop\\imenik.xml";
    private static final String jsonPutanja = "C:\\Users\\NIKOLA\\Desktop\\imenik.json";

    public static void main(String[] args) {

        iscitajXML();
        menu();

    }

    public static void menu() {

        try {
            System.out.println("Imenik - opcije");
            System.out.println("1 - Prikaz imenika");
            System.out.println("2 - Dodavanje kontakta");
            System.out.println("3 - Pretraga");
            System.out.println("4 - Brisanje kontakta");
            System.out.println("0 - Izlazak");
            System.out.println("Unesite opciju:");

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String red;
            while ((red = br.readLine()) != null) {
                if ("0".equals(red)) {
                    upisXML();
                    upisJSON();
                    System.exit(0);
                } else if ("1".equals(red)) {
                    imenik.prikaziImenik();
                } else if ("2".equals(red)) {
                    System.out.println("Ime:");
                    String ime = br.readLine();
                    System.out.println("Prezime:");
                    String prezime = br.readLine();
                    System.out.println("Adresa:");
                    String adresa = br.readLine();
                    Set<String> tel = new HashSet<>();
                    System.out.println("Broj telefona:");
                    tel.add(br.readLine());
                    Set<String> email = new HashSet<>();
                    System.out.println("Email:");
                    email.add(br.readLine());
                    Kontakt k = new Kontakt(ime, prezime, adresa, tel, email);
                    imenik.dodajKontakt(k);
                } else if ("3".equals(red)) {
                    imenik.pregledImenika();
                } else if ("4".equals(red)) {
                    imenik.brisanjeKontakta();
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ImenikDemo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void iscitajXML() {

        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new File(xmlPutanja));

            NodeList nl = doc.getElementsByTagName("kontakt");
            Node n;
            Element e;
            for (int i = 0; i < nl.getLength(); i++) {
                n = nl.item(i);
                if (n.getNodeType() == Node.ELEMENT_NODE) {
                    e = (Element) n;
                    Kontakt kontakt = new Kontakt(e);
                    imenik.dodajKontakt(kontakt);
                }
            }

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(ImenikDemo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void upisXML() {

        try {

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new File(xmlPutanja));
            Element root = doc.getDocumentElement();
            int count = 0;
            int count1 = 0;

            for (Kontakt k : imenik.getImenik()) {

                Element kontakt = doc.createElement("kontakt");
                root.appendChild(kontakt);

                Element ime = doc.createElement("ime");
                ime.setTextContent(k.getIme());
                kontakt.appendChild(ime);

                Element prezime = doc.createElement("prezime");
                prezime.setTextContent(k.getPrezime());
                kontakt.appendChild(prezime);

                Element adresa = doc.createElement("adresa");
                adresa.setTextContent(k.getAdresa());
                kontakt.appendChild(adresa);

                for (String telefon : k.getTel()) {
                        Element tel = doc.createElement("tel");
                        tel.setTextContent(telefon);
                        kontakt.appendChild(tel);
                }

                for (String mail : k.getEmail()) {
                        Element email = doc.createElement("email");
                        email.setTextContent(mail);
                        kontakt.appendChild(email);
                }
            }

            DOMSource source = new DOMSource(doc);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer t = tf.newTransformer();
            StreamResult sr = new StreamResult(new File(xmlPutanja));
            t.transform(source, sr);

        } catch (ParserConfigurationException | SAXException | IOException | TransformerConfigurationException ex) {
            Logger.getLogger(ImenikDemo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(ImenikDemo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void upisJSON() {

        try {

            System.out.println("Upisivanje kontakata u JSON fajl");
            JSONArray jsonNiz = new JSONArray();
            for (Kontakt k : imenik.getImenik()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("ime", k.getIme());
                jsonObject.put("prezime", k.getPrezime());
                jsonObject.put("adresa", k.getAdresa());
                jsonObject.put("tel", k.getTel());
                jsonObject.put("email", k.getEmail());

                jsonNiz.put(jsonObject);
            }

            System.out.println(jsonNiz.toString());

            FileOutputStream fos = new FileOutputStream(new File(jsonPutanja));
            fos.write(jsonNiz.toString().getBytes());
            fos.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ImenikDemo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ImenikDemo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
